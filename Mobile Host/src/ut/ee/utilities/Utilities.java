//   Copyright 2010 Mobile Cloud Lab, University of Tartu
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package ut.ee.utilities;

public class Utilities {
	public static final String LATENCY_TABLE = "tblLatency";
	public static final String BATTERY_TABLE = "tblBattery";
	public static final String BANDWIDTH_TABLE = "tblBandwidth";
	public static final String RELIABILITY_TABLE = "tblReliability";
	public static final String[] TABLES = { LATENCY_TABLE, BATTERY_TABLE,
			BANDWIDTH_TABLE, RELIABILITY_TABLE };

	public static final String F_EXPERIMENT = "experiment";
	public static final int LATENCY = 1;
	public static final int BATTERY = 2;
	public static final int BANDWIDTH = 3;
	public static final int RELIABILITY = 4;

	public static final String F_ID = "id";
	public static final String F_SERVER_ID = "server_id";
	public static final String F_SERVER_TIMESTAMP = "server_timestamp";
	public static final String F_DEVICE_TIMESTAMP = "device_timestamp";
	public static final String F_MESSAGE = "message_text";
	public static final String F_BATCH = "batchid";
	public static final String F_BATTERY = "battery";
	public static final String F_BYTES_SENT = "bytes";
	public static final String F_PACKAGES_SENT = "packages";
	public static final String F_BYTES_RECEIVED = "bytes";
	public static final String F_PACKAGES_RECEIVED = "packages";

	public static final String COMA = ",";
	public static final String SINGLE_QUOTE = "'";
	public static final String END_STATEMENT = ");";

	public static final String INSERT_RELIABILITY = "insert into "
			+ RELIABILITY_TABLE + " (" + F_SERVER_ID + "," + F_SERVER_TIMESTAMP
			+ "," + F_DEVICE_TIMESTAMP + "," + F_MESSAGE + ") values(";

	public static final String INSERT_BATTERY = "insert into " + BATTERY_TABLE
			+ " (" + F_BATCH + "," + F_DEVICE_TIMESTAMP + "," + F_BATTERY
			+ ") values (";

	public static final String INSERT_BANDWIDTH = "insert into "
			+ BANDWIDTH_TABLE + " (" + F_SERVER_ID + "," + F_SERVER_TIMESTAMP
			+ "," + F_DEVICE_TIMESTAMP + "," + F_BYTES_SENT + ","
			+ F_PACKAGES_SENT + "," + F_BYTES_RECEIVED + ","
			+ F_PACKAGES_RECEIVED + "," + F_MESSAGE + ") values(";

	public static final String INSERT_LATENCY = "insert into " + LATENCY_TABLE
			+ " ("+F_SERVER_ID+","+F_BATCH+","+F_SERVER_TIMESTAMP+","+F_DEVICE_TIMESTAMP+","+F_MESSAGE+") values(";

	private static String CREATE_LATENCY_TABLE = "create table "
		+ LATENCY_TABLE + " (" + F_ID
		+ " integer primary key autoincrement, " + F_SERVER_ID
		+ " integer, " + F_SERVER_TIMESTAMP + " text, "
		+ F_DEVICE_TIMESTAMP + " text, " + F_MESSAGE + " text); ";
	
	private static String CREATE_RELIABILITY_TABLE = "create table "
			+ LATENCY_TABLE + " (" + F_ID
			+ " integer primary key autoincrement, " + F_SERVER_ID
			+ " integer, " + F_BATCH + " integer, " + F_SERVER_TIMESTAMP
			+ " text, " + F_DEVICE_TIMESTAMP + " text, " + F_MESSAGE
			+ " text); ";

	private static String CREATE_BANDWIDTH_TABLE = "create table "
			+ LATENCY_TABLE + " (" + F_ID
			+ " integer primary key autoincrement, " + F_SERVER_ID
			+ " integer, " + F_SERVER_TIMESTAMP + " text, "
			+ F_DEVICE_TIMESTAMP + " text, " + F_BYTES_SENT + " real, "
			+ F_PACKAGES_SENT + " real, " + F_BYTES_RECEIVED + " real, "
			+ F_PACKAGES_RECEIVED + " real, " + F_MESSAGE + " text); ";

	private static String CREATE_BATTERY_TABLE = "create table "
			+ LATENCY_TABLE + " (" + F_ID
			+ " integer primary key autoincrement, " + F_BATCH + " integer, "
			+ F_DEVICE_TIMESTAMP + " real, " + F_BATTERY + " real); ";

	public static final String[] CREATE_TABLE_STATEMENTS = {
			CREATE_LATENCY_TABLE, CREATE_BATTERY_TABLE, CREATE_BANDWIDTH_TABLE,
			CREATE_RELIABILITY_TABLE };

}
