//   Copyright 2010 Mobile Cloud Lab, University of Tartu
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package ut.ee.sroid;

public class profileHtml {
	static String htmlSite;
		
	public static String getHtml(){
		htmlSite = "<html>" +
						"<head>" +
							"<title>Hello, my name is P�tris</title>" +
						"</head>" +
						"<body>" +
							"<center><H1>P�tris Halapuu's Profile</H1></center>" +
							"<H2>Info</H2>" +
							"<img src="+ "http://photos.geni.com/p13/8f/96/f3/41/53444839050a456a/foto_medium.jpg" + 
							" alt="+"Me"+" /><br>" +
							"Name: P�tris Halapuu" +
							"<br>Birth date: 31.07.1991<br>" +
							"Country: Estonia <br>" +
							"E-mail: p2tris@gmail.com<br>" +
							"Phone number: +372 5669 3134<br>" +
							"Web & blog: <a href=" + "http://patris.halapuu.com" + "> patris.halapuu.com</a><br>" +
							"My location: <a href="+ "../location" +" >Get my coordinates</a>" +
							"<H2>About me</H2>" +
							"I am programmer-folk dancer. For more information contact me or check out my blog!" +
						"</body>" +
					"</html>";
		return htmlSite;
	}
}
