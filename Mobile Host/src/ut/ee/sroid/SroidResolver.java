//   Copyright 2010 Mobile Cloud Lab, University of Tartu
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package ut.ee.sroid;

import java.io.IOException;
import java.io.InputStream;

import org.apache.felix.framework.Felix;
import org.apache.http.HttpException;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import ut.ee.mds.SroidService;
import ut.ee.mh.Sroid;

import android.R;
import android.app.Activity;
import android.content.Context;
import android.util.Log;

public class SroidResolver {
	static Felix mFelix;
	static Activity activity;
	static String message = "";

	public SroidResolver(Felix felix, Activity main) {
		mFelix = felix;
		activity = main;
	}

	public String resolve(String serviceDescription, String method,
			final SroidRequest request, final SroidResponse response)
			throws InvalidSyntaxException {
		ServiceReference[] refs = mFelix.getBundleContext()
				.getServiceReferences(SroidService.class.getName(),
						"(Service="+serviceDescription+")");
		if (refs != null) {
			final SroidService sroidService = (SroidService) mFelix
					.getBundleContext().getService(refs[0]);
			Context context = activity.getApplicationContext();
			sroidService.doCreate(activity, context);
			Log.v("DEBUG-OWN", "before enter the while");
			// while (true)
			{
				message = sroidService.checkAvailability();
				try {
					sroidService.doGEt(request, response);
				} catch (HttpException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(message);
				Log.v("DEBUG-OWN", message);
			}
		}
		return "";
	}
	
	public String resolve(String serviceDescription, String method,
			final SroidRequest_ request, final SroidResponse_ response)
			throws InvalidSyntaxException {
		ServiceReference[] refs = mFelix.getBundleContext()
				.getServiceReferences(SroidService.class.getName(),
						"(Service="+serviceDescription+")");
		if (refs != null) {
			final SroidService sroidService = (SroidService) mFelix
					.getBundleContext().getService(refs[0]);
			Context context = activity.getApplicationContext();
			sroidService.doCreate(activity, context);
			Log.v("DEBUG-OWN", "before enter the while");
			{
				try {
					response.write("Hola mundo");
				    response.close();
				} catch (HttpException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(message);
				Log.v("DEBUG-OWN", message);
			}
		}
		return "";
	}
	
}
