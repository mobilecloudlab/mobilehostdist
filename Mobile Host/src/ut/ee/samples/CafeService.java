//   Copyright 2010 Mobile Cloud Lab, University of Tartu
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package ut.ee.samples;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class CafeService {

	private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 1; // in
	// Meters
	private static final long MINIMUM_TIME_BETWEEN_UPDATES = 1000; // in
	// Milliseconds
	static String SERVICE_TAG = "gps";
	static String CITY_TAG = "city";
	static String COUNTRY_TAG = "estonia";
	static String WIRELESS_TAG = "wireless";
	static String GPS_DEVICE_TAG = "gps_device";
	static String text = "";

	protected LocationManager locationManager;
	protected Activity activity;
	protected Context context;

	//58.380152,26.71551
	private String messageCafe = "{   \"status\": \"OK\", \"num_results\": 1, \"results\": [ { \"id\": \"2827\", \"lat\": \"58.380152\", \"lng\": \"26.71551\", \"elevation\": \"1737\", \"title\": \"Kissing Students Caffee\", \"distance\": \"1\", \"has_detail_page\": \"1\", \"webpage\": \"http://androidserver.mcrlabs.net:9999/file\" } ] }";

	private double[] getLocation() {
		double[] ret = new double[2];
		ret[0]=0;
		ret[1]=0;
		Location location = locationManager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		if (location == null)
			location = locationManager
					.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		if (location != null) {
			String message = String
					.format("\"Longitude\": \"%1$s\", \"Latitude\": \"%2$s\", \"timestamp\": \"%3$s\"",
							location.getLongitude(), location.getLatitude(),
							System.currentTimeMillis());
			ret[0]=location.getLongitude();
			ret[1]=location.getLatitude();
			return ret;
		}
		return ret;
	//	return "Location services not available";
	}
	
	private class SroidLocationListener implements LocationListener {
		@Override
		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
			String message = String.format(
					"New Location \n Longitude: %1$s \n Latitude: %2$s",
					location.getLongitude(), location.getLatitude());

		}

		@Override
		public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderDisabled(String arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderEnabled(String arg0) {
			// TODO Auto-generated method stub

		}

	}

	
	public String getJSON() {
		return messageCafe;
	}

}
