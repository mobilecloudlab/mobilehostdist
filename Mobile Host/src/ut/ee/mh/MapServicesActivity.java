//   Copyright 2010 Mobile Cloud Lab, University of Tartu
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
// by Patris Halapuu, University of Tartu
package ut.ee.mh;

import java.util.HashMap;
import java.util.List;

import javax.jmdns.ServiceInfo;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.TextView;

public class MapServicesActivity extends MapActivity{
    /** Called when the activity is first created. */
	
	MapController mapController;
	MapView mapView;
	
	List<Overlay> mapOverlays;
	GMapsItemizedOverlay itemizedoverlay;
	
	List<Overlay> mapServiceOverlays;
	static GMapsItemizedOverlay itemizedServiceOverlay;
	
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
        
        mapView = (MapView) findViewById(R.id.mapView);
        mapView.setBuiltInZoomControls(true);
        mapView.setSatellite(true);
        
        mapController = mapView.getController();
        mapController.setZoom(16);
        mapController.setCenter(new GeoPoint((int)(58.378161 * 1E6),(int)(26.714864 * 1E6)));
        
        LocationManager locationManager;
    	String context = Context.LOCATION_SERVICE;
    	locationManager = (LocationManager) getSystemService(context);    
    	
    	String provider = LocationManager.GPS_PROVIDER;
    	Location currentLocation = 	locationManager.getLastKnownLocation(provider);
    	
    	mapOverlays = mapView.getOverlays();
    	Drawable drawable = this.getResources().getDrawable(R.drawable.green_pin);
    	itemizedoverlay = new GMapsItemizedOverlay(drawable, this);
    	mapOverlays.add(itemizedoverlay);
    	
    	mapServiceOverlays = mapView.getOverlays();
    	Drawable drawableService = this.getResources().getDrawable(R.drawable.google_pin);
    	itemizedServiceOverlay = new GMapsItemizedOverlay(drawableService, this);
    	mapServiceOverlays.add(itemizedServiceOverlay);
    	
    	updateNewServices(58.378161, 26.714864, "CS Institute", "This is where the real work is done", "No service here!");
    	
    	LocationListener locationListener = new LocationListener() {
    	    public void onLocationChanged(Location location) {
    	    	updateWithNewLocation(location);
    	    	GeoPoint point = new GeoPoint((int)(location.getLatitude()*1E6), 
    	    									(int)(location.getLongitude()*1E6));
    	    	mapController.setCenter(point);
    	    	
    	    }

    	    public void onStatusChanged(String provider, int status, Bundle extras) {}
    	    public void onProviderEnabled(String provider) {}
    	    public void onProviderDisabled(String provider) {}
    	  };

    	locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    	
    	updateWithNewLocation(currentLocation);    	
    	    	
    	for (Object info: Sroid.getPins()){
			ServiceInfo pins = (ServiceInfo) info;
			
			java.util.Enumeration<String> propsNames = pins.getPropertyNames();
			HashMap<String, String> hashProps = new HashMap<String, String>();
			while (propsNames.hasMoreElements()){
				String propsName = propsNames.nextElement();
				String value = pins.getPropertyString(propsName);
				hashProps.put(propsName, value);
			}
			
			double lat = Double.parseDouble(hashProps.get("Latitude"));
			double lon = Double.parseDouble(hashProps.get("Longtitude"));
			String title = hashProps.get("Title");
			String text = hashProps.get("Text");
			String ip = hashProps.get("IP");
			
			MapServicesActivity.updateNewServices(lat, lon, title, text, ip);					
		}
    	        
    }

    TextView locationText;
    
    private void updateWithNewLocation(Location location){
    	String latlong;
    	
    	locationText = (TextView) findViewById(R.id.textView);
    	if (location != null ) {
    		double lat = location.getLatitude();
    		double lng = location.getLongitude();
    		latlong = "Lat: " + lat + "\nLong: " + lng;
		} else {
			latlong = "No location found";
		}
    	locationText.setText("Your current position is: \n" + latlong);
    	
    	
    	OverlayItem overlayitem;
    	if (location == null) {
    		overlayitem = new OverlayItem(new GeoPoint((int)(58.384 * 1E6), (int)(26.706 * 1E6)), "Hey buddy!", "You were here!");
		} else {
        	overlayitem = new OverlayItem(new GeoPoint((int)(location.getLatitude()*1E6), (int)(location.getLongitude()*1E6)), "Hey buddy!", "You are here!");
		}
    	itemizedoverlay.deleteOverlay();
    	itemizedoverlay.addOverlay(overlayitem);
    }
    
    static void updateNewServices(double lat, double lon, String title, String text, String ip){
    	OverlayItem servicePins;
    	
    	servicePins = new OverlayItem(new GeoPoint((int)(lat * 1E6), (int)(lon * 1E6)), title, text + " " + ip);
    	
    	itemizedServiceOverlay.addOverlay(servicePins);

    }
    
	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
}