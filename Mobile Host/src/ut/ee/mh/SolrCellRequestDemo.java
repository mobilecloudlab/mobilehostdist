//   Copyright 2010 Mobile Cloud Lab, University of Tartu
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package ut.ee.mh;

import java.io.File;
import java.io.IOException;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.request.ContentStreamUpdateRequest;
import org.apache.solr.common.util.NamedList;

public class SolrCellRequestDemo {
	  public static void main () throws IOException, SolrServerException {
	    SolrServer server = new HttpSolrServer("http://mcrlabs.net:8983/solr");
	    ContentStreamUpdateRequest req = new ContentStreamUpdateRequest("/update/extract");
	    req.addFile(new File("/sdcard/fileshare/uploads/services.txt"), null);
	    NamedList<Object> result = server.request(req);
	    System.out.println("Result: " + result);
	  }
}