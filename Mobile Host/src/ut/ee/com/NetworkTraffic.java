//   Copyright 2010 Mobile Cloud Lab, University of Tartu
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package ut.ee.com;

import android.content.Context;
import android.net.TrafficStats;

// This class was made for network traffic overview
public class NetworkTraffic {

	//TrafficStats stats = new TrafficStats();
	Context mContext;

	public NetworkTraffic(Context context) {
		mContext = context;
	}

	public long getBytesReceived() {
		return TrafficStats.getTotalRxBytes();
	}

	public long getPackagesReceived() {
		return TrafficStats.getTotalRxPackets();
	}

	public long getBytesSent() {
		return TrafficStats.getTotalTxBytes();
	}

	public long getPackagesSent() {
		return TrafficStats.getTotalTxPackets();
	}

}
