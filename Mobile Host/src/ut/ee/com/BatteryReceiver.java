//   Copyright 2010 Mobile Cloud Lab, University of Tartu
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package ut.ee.com;

import ut.ee.db.DBManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.util.Log;

// This class was made for testing battery life. Entrys to db are made about battery %
public class BatteryReceiver extends BroadcastReceiver {

	private static final String TAG = "BatteryLife";
	DBManager db;
	Context mContext;
	int batchID;

	public BatteryReceiver(Context context) {
		// TODO Auto-generated constructor stub
		mContext = context;
		db =new  DBManager(mContext);		
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
		int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 100);
		Log.i(TAG, "level: " + level + "; scale: " + scale);
		int percent = (level * 100) / scale;
		db.open();
		db.createEntryForBattery(batchID,System.currentTimeMillis(),percent);
		db.close();
		Log.v("NS", "Entry created inside battery table - Battery level "+percent+"%.");
	}

}
