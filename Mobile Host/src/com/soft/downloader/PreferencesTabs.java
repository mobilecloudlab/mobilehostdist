package com.soft.downloader;

import android.app.TabActivity;
import android.os.Bundle;
import android.view.Window;
import android.widget.TabHost;
import android.widget.TextView;
import android.content.Intent;
import ut.ee.mh.R;

public class PreferencesTabs extends TabActivity {
		
	static TextView mRightText = null;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); 
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
                  
        final TabHost tabHost = getTabHost();        

        tabHost.addTab(tabHost.newTabSpec("tab_download")
                .setIndicator(getString(R.string.tab_download))
                .setContent(new Intent(this, DownloadPreferencesScreen.class)));      
        
   
        
        Bundle bundle = this.getIntent().getExtras();
        if(bundle != null){
        	String currentTab = bundle.getString("CurrentTab");
	        if(currentTab != null)
	        {
	        	tabHost.setCurrentTabByTag(currentTab);
	        }
        }
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title);

        DownloaderApp.DownloadServiceMode = false;        
    } 
    
    public static void SetRightCustomTitle(String text){
    	if(mRightText != null)
    		mRightText.setText(text);    	
    }
    public static String GetRightCustomTitle(){
    	String result = new String("");
    	if(mRightText != null)    	
    		result = (String)mRightText.getText();
    	return result;
    }
}
