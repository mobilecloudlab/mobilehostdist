package com.soft.downloader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;


import com.soft.downloader.DownloadService.Controller.ControllerState;
import com.soft.file.FileManagerActivity;

import android.app.Activity;
import android.app.Application;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.util.AttributeSet;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.LayoutInflater.Factory;
import android.webkit.WebViewDatabase;
import android.widget.TextView;
import ut.ee.mh.R;

public class DownloaderApp extends Application {	
		
   	//Enumerations
	public enum ActivityResultType {
		RESULT_OK(-1),RESULT_CANCELED(0),RESULT_FIRST_USER(1),
		RESULT_MAIN(2), RESULT_DOWNLOADER(3), RESULT_EXIT(4);
		
		private int mCode;
		private ActivityResultType(int code) {mCode = code;}
		
		public int getCode() {return mCode;}
		public static ActivityResultType getValue(int code){
			ActivityResultType result = RESULT_OK;
			for (ActivityResultType value : ActivityResultType.values()) {
				if(value.getCode() == code){result = value; break;}					
			}
			return result;
		}
	}	

	//Constants
	public static final String	TAG = "Torrent";

	//Default Download Preferences constants
	public static final int		ListenPort = 54312;
	public static final int		UploadLimit = 0;
	public static final int		DownloadLimit = 0;	
	public static final boolean UPNP = false;
	public static final boolean LSD = false;
	public static final boolean NATPMP = false;
	
	public static final int		StorageModeCompactMB = 750; //MB
	
	public static final int		ProxyType = 0; //none
	public static final String 	HostName =  new String("");
	public static final int 	PortNumber = 0;
	public static final String 	UserName = new String("");
	public static final String 	UserPassword = new String("");
	public static final String 	DefaultTorrentSavePath = Environment.getExternalStorageDirectory()+"/";
	
	//Variables
	public static volatile boolean	DownloadServiceMode = true;
	public static volatile boolean	StartFinalClose = false;
	
	public static String	TorrentFullFileName = new String("undefined");
	
	public static String	CookieData = new String("");
	public static String	FeedUrl = new String("");
	public static String	SearchUrl = new String("");
	public static boolean	ExitState = false;	

	//public static boolean 	ActivateTorrentFileList=false;
	
	@Override
	public void onCreate() {
		super.onCreate();
		try{
			  
			WebViewDatabase webViewDB = WebViewDatabase.getInstance(this);
			if (webViewDB!=null){
				startService(new Intent(this, DownloadService.class));
			}
			StartServiceActivity(getApplicationContext());
			RestoryTorrentsFromDB(getApplicationContext());
		}catch(Exception ex){
			Log.e(TAG, ex.toString());
		}
	}		
	static public void RestoryTorrentsFromDB(Context context){
		TorrentsSQLHelper torrentsDB = new TorrentsSQLHelper(context);
		try {
	        SQLiteDatabase db = torrentsDB.getReadableDatabase();
	        Cursor cursor = db.query(TorrentsSQLHelper.TABLE, null, null, null, null, null, null);
	        //startManagingCursor(cursor);        
	        while (cursor.moveToNext()) {
	          //long id = cursor.getLong(0);
	          int progress = cursor.getInt(1);
	          int progressSize = cursor.getInt(2);
	          int storageMode = cursor.getInt(3);
	          String savePath = cursor.getString(4);
	          String fileName = cursor.getString(5);
	          TorrentsList.AddTorrent(context, fileName, progress, progressSize, storageMode, savePath);
	        }
	     } catch (SQLiteException sqle){
	    	 Log.w(TAG, "Error while opening database", sqle);
		 } finally {
			torrentsDB.close();
		 }
	}	
	static public void StoreTorrentsToDB(Context context) {
		TorrentsSQLHelper torrentsDB = new TorrentsSQLHelper(context);
		try {
			SQLiteDatabase db = torrentsDB.getWritableDatabase();
	        ContentValues values = new ContentValues();
	        db.delete(TorrentsSQLHelper.TABLE, null, null);	        	
	    	for(int i=0;i<TorrentsList.Torrents.size();i++){
	    		TorrentContainer tc = TorrentsList.Torrents.get(i);
	            values.put(TorrentsSQLHelper.PROGRESS, tc.Progress);
	            values.put(TorrentsSQLHelper.PROGRESS_SIZE, tc.ProgressSize);
	            values.put(TorrentsSQLHelper.STORAGE_MODE, tc.StorageMode);
	            values.put(TorrentsSQLHelper.SAVE_PATH, tc.SavePath);
	            values.put(TorrentsSQLHelper.FILE, tc.Name);
	            db.insert(TorrentsSQLHelper.TABLE, null, values);
	    	}
	     } catch (SQLiteException sqle){
	    	 Log.w(TAG, "Error while opening database", sqle);
		 } finally {
			torrentsDB.close();
		 }
    }

    static public void MainScreen(Activity activity){
    	activity.setResult(DownloaderApp.ActivityResultType.RESULT_MAIN.getCode());
    	activity.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    	if(DownloadServiceMode)
    		OpenMainScreen(activity);
    	activity.finish();
    }

    static public void ToDownloaderActivity(Activity activity){
		activity.setResult(DownloaderApp.ActivityResultType.RESULT_DOWNLOADER.getCode());
		activity.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
		activity.finish();
    }
    /////////////////
    static public void OpenDownloadPreferencesScreen(Activity activity){
    	Intent intent = new Intent(Intent.ACTION_VIEW);
    	intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
    	intent.setClassName(activity, DownloadPreferencesScreen.class.getName());
    	activity.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    	activity.startActivityForResult(intent, 0);
    }

    
    static public void OpenDownloaderActivity(Activity activity){
    	Intent intent = new Intent(Intent.ACTION_VIEW);
    	intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
    	intent.setClassName(activity, TorrentsList.class.getName());
    	activity.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    	activity.startActivityForResult(intent, 0);
    }
    

    
    /////////////////    
    
    static public void OpenMainScreen(Activity activity){
    	Intent intent = new Intent(Intent.ACTION_VIEW);
    	intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
    	intent.setClassName(activity, MainScreen.class.getName());
    	activity.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    	activity.startActivity(intent);
    }

    static public void OpenTorrentDownloadActivity(Activity activity){
    	Intent intent = new Intent(Intent.ACTION_VIEW);
    	intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
    	intent.setClassName(activity, DownloadService.Controller.class.getName());
    	activity.startActivityForResult(intent, 0);
    }



    static public void FileManagerActivity(Activity activity){
    	Intent intent = new Intent(Intent.ACTION_VIEW);
    	intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
    	intent.setClassName(activity, FileManagerActivity.class.getName());
    	activity.startActivityForResult(intent,0); 
    	activity.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }
 
    static public void CloseApplication(Activity activity){
    	DownloaderApp.ExitState = true;
    	activity.setResult(DownloaderApp.ActivityResultType.RESULT_EXIT.getCode());
    	activity.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    	activity.finish();
    }
        
    static public void FinalCloseApplication(final Activity activity){
    	DownloaderApp.ExitState = true;
    	if(!StartFinalClose){
    		StartFinalClose = true;
	    	final ProgressDialog dialog = ProgressDialog.show(activity, "", activity.getString(R.string.progress_close), true, false);
	    	StoreTorrentsToDB(activity);
	    	TorrentsList.FinalRemoveTorrents();
			activity.stopService(new Intent(activity,DownloadService.class));
			NotificationManager nm = (NotificationManager)activity.getSystemService(NOTIFICATION_SERVICE);
	 		nm.cancelAll();
	    	SharedPreferences prefs =  activity.getSharedPreferences(DownloadService.Controller.class.getName(), MODE_PRIVATE);
	    	SharedPreferences.Editor ed = prefs.edit();
	        ed.putInt(ControllerState.class.getName(), ControllerState.Undefined.ordinal());
	        ed.commit();

	        final Handler handler = new Handler() {
	            @Override
	            public void handleMessage(Message msg) {
	                try{
	                	dialog.dismiss();
	                }catch(Exception ex){}
	            	DownloadService.LibTorrents.AbortSession();
	                activity.moveTaskToBack(false);
	        	  	Process.killProcess(Process.myPid());
	            }
	        };        
	        // Start lengthy operation in a background thread
	        new Thread(new Runnable() {
	            public void run() {
			try{
			        DownloaderApp.ClearCache(activity);
			}catch(Exception ex){}
			handler.sendEmptyMessage(0);
	            }
	        }).start();
    	}
	}    
 
    static public void ClearCache(Context context){    	
    	try { 
    		File dir = context.getCacheDir(); 
    		if (dir != null && dir.isDirectory()) { 
    			deleteDir(dir); 
    		} 
    	} catch (Exception e){}     	
    }
    
    public static boolean deleteDir(File dir) { 
    	if (dir != null && dir.isDirectory()) { 
    		String[] children = dir.list(); 
    		for (int i = 0; i < children.length; i++) { 
    			boolean success = deleteDir(new File(dir, children[i])); 
    			if (!success) { 
    				return false; 
    			} 
    		} 
    	}
    	// The directory is now empty so delete it 
    	return dir.delete();
    }
    
	static public boolean CopyFile(File oldFile, File newFile) {
		boolean result = false;
		final int COPY_BUFFER_SIZE = 32 * 1024;
		try {
			FileInputStream input = new FileInputStream(oldFile);
			FileOutputStream output = new FileOutputStream(newFile);
		
			byte[] buffer = new byte[COPY_BUFFER_SIZE];
			
			while (true) {
				int bytes = input.read(buffer);
				
				if (bytes <= 0) {
					break;
				}				
				output.write(buffer, 0, bytes);
			}			
			output.close();
			input.close();
			result = true;	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	} 
	
	public static boolean CheckService(Context context){
		boolean result = false;
  		String info = System.getProperty("ServiceActivity", "no");
        if(info.equals("yes"))
        	result = true;
        return result;
	}

	public static void StartServiceActivity(Context context){ 
        try {
			String keyPkg = "com.soft.downloader"; 
      	    Intent intent = new Intent();
    	    intent.setClassName(keyPkg, keyPkg + ".ServiceActivity");
	        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        context.startActivity(intent);
        }catch(Exception ex){}
	} 
		

	
	public static void SetMenuBackground(final Activity activity){
		activity.getLayoutInflater().setFactory( new Factory() {  
	        public View onCreateView(String name, Context context, AttributeSet attrs) {
	            if ( name.equalsIgnoreCase( "com.android.internal.view.menu.IconMenuItemView" ) ) {
	                try { // Ask our inflater to create the view  
	                    LayoutInflater f = activity.getLayoutInflater();  
	                    final View view = f.createView( name, null, attrs );  
	                    /* The background gets refreshed each time a new item is added the options menu.  
	                    * So each time Android applies the default background we need to set our own  
	                    * background. This is done using a thread giving the background change as runnable 
	                    * object */
	                    new Handler().post( new Runnable() {  
	                        public void run () {  
	                            // sets the background color   
	                            view.setBackgroundResource( R.drawable.ic_tab_unactive_small);
	                            // sets the text color              
	                            //((TextView) view).setTextColor(Color.CYAN);
	                            ((TextView) view).setTextColor(activity.getResources().getColor(R.color.gold));
	                            // sets the text size              
	                            //((TextView) view).setTextSize(16);
	            }
	                    } );  
	                return view;
	            }
	        catch ( InflateException e ) {}
	        catch ( ClassNotFoundException e ) {}  
	   } 
	    return null;
	        }
	});
	}

}