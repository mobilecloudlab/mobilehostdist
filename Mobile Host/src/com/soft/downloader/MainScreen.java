package com.soft.downloader;

import ut.ee.mh.R;

import com.soft.downloader.DownloaderApp;
import com.soft.downloader.DownloadPreferencesScreen.MenuType;
import com.soft.downloader.DownloaderApp.ActivityResultType;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

public class MainScreen extends Activity {	
	
		private int mBackPressedCount = 0;

	    @Override
	    public void onCreate(Bundle icicle) {
	        super.onCreate(icicle);
	        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
	        
	        setContentView(R.layout.main_screen);
	        
	        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title);

	        startService(new Intent(this, DownloadService.class));
	        DownloaderApp.DownloadServiceMode = false;        	        
	    }
	    
	    @Override
	    protected void onResume() {
	    	super.onResume();
	    	mBackPressedCount = 0;
	        if(DownloaderApp.ExitState) DownloaderApp.CloseApplication(this);
	    }
	    				
	    @Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			switch(ActivityResultType.getValue(resultCode))
			{
			case RESULT_DOWNLOADER:{
				DownloaderApp.OpenDownloaderActivity(this);
			} break;
			case RESULT_MAIN:{			
			} break;
			case RESULT_EXIT:
				DownloaderApp.FinalCloseApplication(this);
			default:{
			} break;
			};
		}
		
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			super.onCreateOptionsMenu(menu);
			menu.add(Menu.NONE, MenuType.FileManager.ordinal(), MenuType.FileManager.ordinal(), R.string.menu_file_manager);
			menu.add(Menu.NONE, MenuType.Exit.ordinal(), MenuType.Exit.ordinal(), R.string.menu_exit);			
		    DownloaderApp.SetMenuBackground(this); 
			return true;
		}
		
		@Override
		public boolean onMenuItemSelected(int featureId, MenuItem item) {
			super.onMenuItemSelected(featureId, item);
			MenuType type = MenuType.values()[item.getItemId()];
			switch(type)
			{
			case FileManager:{
				DownloaderApp.FileManagerActivity(this);
			} break;
			case Exit:{
				DownloaderApp.FinalCloseApplication(this);
			} break;
			}
			return true;
		}
		@Override 
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			if(keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)
	    		if(mBackPressedCount == 0){
	    			Toast.makeText(this, getString(R.string.press_back_to_exit), Toast.LENGTH_SHORT).show();
	    			mBackPressedCount++;
	    			return true;
	    		} else {
	    			DownloaderApp.FinalCloseApplication(this);
	    		}
			return super.onKeyDown(keyCode,event); 
		}
						
		public void OnClickMainButtonDownload(View v){
			DownloaderApp.OpenDownloaderActivity(this);			
		}
		public void OnClickMainButtonSettings(View v){
			DownloaderApp.OpenDownloadPreferencesScreen(this);			
		}
		
				
}
