package peerdroid.sample;

import java.util.ArrayList;

import peerdroid.sample.handler.RandomMessagesHandler;
import peerdroid.sample.handler.SocketServerHandler;
import peerdroid.sample.info.NetworkInformation;
import peerdroid.sample.neighbours.Peer;
import peerdroid.sample.service.JXTAService;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import peerdroid.sample.handler.*;

/**
 * 
 * Sample Application that shows how we can user PeerDroid on Android platform.
 * 
 * @author Marco Picone picone.m@gmail.com
 * @created 27/08/2009
 */
public class PeerDroidSample {

	public static String TAG = "PEERDROID-SAMPLE";

	/* PeerDroid Sample - GUI Elements */
	private Button sendButton = null;
	public static TextView messagesTextView= null;
	private static JXTAService managerJXTA = null;
	private EditText messageEditText;

	/* Message that will be send to other peers */
	public static String outgoingMessage = "Hello World !";

	/* Check if */
	public static boolean programClosed = false;

	// NetworkInformation object
	private NetworkInformation networkInfo = new NetworkInformation();

	/* List of peers received from the RDV */
	private ArrayList<Peer> peerList = new ArrayList<Peer>();

	// Listener for Send Button

	/* Dialog Window used to show information about connection to JXTA NetWork */
	// public static IntroDialog introDialog;

	/* Dialog Windows used to show information about NetWork Status */
	// private NetworkInfoDialog infoDialog;

	/* Dialog Window used to show a Graph for Network Speed */
	// private GraphDialog dialogGraph;

	/* Dialog that shows the peerlist received from the RDV */
	// private PeerListDialog peerListDialog;

	/* Thread to manage SocketServer of the client */
	public Thread serverThread = null;

	/* Thread to manage random messages to available peers */
	public Thread randomMessagesThread = null;

	/* Thread of JXTA Connection */
	private Thread jxtaThread;

	/* Android Handler for PeerDroidSample */
	public static Handler handler = new Handler();
	public static Activity activity;
	
	public PeerDroidSample(Activity p_activity){
		this.activity = p_activity;
		
	}
	
	public void start() {
		jxtaThread = new Thread() {
			public void run() {
				managerJXTA = new JXTAService("Ubuntu-Piko" + this.hashCode(),
						"principal", "password", PeerDroidSample.activity.getFileStreamPath("jxta"),PeerDroidSample.activity);
				managerJXTA.peerList = peerList;
				managerJXTA.startSearchingPeers();

				serverThread = new Thread(new SocketServerHandler(
						managerJXTA.getSocketService()),
						"Connection Handler Thread");
				serverThread.start();

				randomMessagesThread = new Thread(new RandomMessagesHandler(
						managerJXTA), "Random Message Thread");
				randomMessagesThread.start();
			}
		};
		jxtaThread.start();

	}

 	public void stop() {
		// Stop JXTA Network
		managerJXTA.stop();
		// Suspend different Threads
		serverThread.suspend();
		randomMessagesThread.suspend();
		jxtaThread.suspend();

		Log.d(TAG, "onDestroy CALL !");
	}
}