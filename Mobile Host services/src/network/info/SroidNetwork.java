//   Copyright 2010 Mobile Cloud Lab, University of Tartu
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package network.info;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.RequestLine;
import org.apache.http.impl.DefaultHttpServerConnection;

import ut.ee.mds.SroidService;
import ut.ee.sroid.SroidRequest;
import ut.ee.sroid.SroidResponse;
import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;


public class SroidNetwork implements SroidService {
	
	protected ConnectivityManager conMan;
	protected Activity activity;
	protected Context context;
	
	public String getHTMLHeader() {
		return "<html><head><title>Network information</title></head><body>";
	}

	public String getHTMLFooter() {
		return "</body></html>";
	}
	
	@Override
	public void onAccelerationChanged(float x, float y, float z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onShake(float force) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doCreate(Activity activity, Context context) {
		this.activity = activity;
		this.context = context;
		conMan = (ConnectivityManager) activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		
	}
	
	private String getNInfo() {
		String message = "No connection";
		//mobile
		State mobile = conMan.getNetworkInfo(0).getState();
		//wifi
		State wifi = conMan.getNetworkInfo(1).getState();
		
		if (mobile == NetworkInfo.State.CONNECTED || mobile == NetworkInfo.State.CONNECTING) {
		    message = "Mobile";
		} else if (wifi == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTING) {
		    message = "WiFi";
		}		
		return message;
		
	}

	@Override
	public void doGEt(SroidRequest request, SroidResponse response){
		String message = "{"+getNInfo()+"}";
		response.write(message);
		
		try {
			response.close();
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void doPost(SroidRequest request, SroidResponse response)
			throws HttpException, IOException {
		// TODO Auto-generated method stub
		String message = getHTMLHeader();
		message = message + "<p> Not implemented yet </p>" + getHTMLFooter();
		response.write(message);
		response.close();
		
	}

	@Override
	public void doDelete(SroidRequest request, SroidResponse response)
			throws HttpException, IOException {
		// TODO Auto-generated method stub
		String message = getHTMLHeader();
		message = message + "<p> Not implemented yet </p>" + getHTMLFooter();
		response.write(message);
		
	}

	@Override
	public void doPut(SroidRequest request, SroidResponse response)
			throws HttpException, IOException {
		// TODO Auto-generated method stub
		String message = getHTMLHeader();
		message = message + "<p> Not implemented yet </p>" + getHTMLFooter();
		response.write(message);
	}

	@Override
	public boolean processRequest(DefaultHttpServerConnection serverConnection,
			HttpRequest request, RequestLine requestLine) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String checkAvailability() {
		// TODO Auto-generated method stub
		String message = getHTMLHeader();
		message = message + "<p> " + getNInfo() + "</p>";
		message = message + "<br><br>";
		message = message + "<p> Works! </p>" + getHTMLFooter();
		return message;
	}

	
	@Override
	public void doGetFile(InputStream f, SroidRequest request,
			SroidResponse response) throws HttpException, IOException {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public HashMap<String, String> getDescriptors() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	
}
