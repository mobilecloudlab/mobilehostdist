//   Copyright 2010 Mobile Cloud Lab, University of Tartu
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package mobile.info;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.RequestLine;
import org.apache.http.impl.DefaultHttpServerConnection;

import ut.ee.mds.SroidService;
import ut.ee.sroid.SroidRequest;
import ut.ee.sroid.SroidResponse;
import android.app.Activity;
import android.content.Context;


public class SroidMobInf implements SroidService {
	
	protected Activity activity;
	protected Context context;
	
	public String getHTMLHeader() {
		return "<html><head><title>Network information</title></head><body>";
	}

	public String getHTMLFooter() {
		return "</body></html>";
	}
	
	@Override
	public void onAccelerationChanged(float x, float y, float z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onShake(float force) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doCreate(Activity activity, Context context) {
		this.activity = activity;
		this.context = context;
		
	}
	
	private String getMInfo() {
		String message ="Debug-infos:";
		message += "\n OS Version: " + System.getProperty("os.version") + "(" + android.os.Build.VERSION.INCREMENTAL + ")";
		message += "\n OS API Level: " + android.os.Build.VERSION.SDK;
		message += "\n Device: " + android.os.Build.DEVICE;
		message += "\n Model (and Product): " + android.os.Build.MODEL + " ("+ android.os.Build.PRODUCT + ")";
		return message;
		
	}

	@Override
	public void doGEt(SroidRequest request, SroidResponse response){
		String message = "{"+getMInfo()+"}";
		response.write(message);
		
		try {
			response.close();
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void doPost(SroidRequest request, SroidResponse response)
			throws HttpException, IOException {
		// TODO Auto-generated method stub
		String message = getHTMLHeader();
		message = message + "<p> Not implemented yet </p>" + getHTMLFooter();
		response.write(message);
		response.close();
		
	}

	@Override
	public void doDelete(SroidRequest request, SroidResponse response)
			throws HttpException, IOException {
		// TODO Auto-generated method stub
		String message = getHTMLHeader();
		message = message + "<p> Not implemented yet </p>" + getHTMLFooter();
		response.write(message);
		
	}

	@Override
	public void doPut(SroidRequest request, SroidResponse response)
			throws HttpException, IOException {
		// TODO Auto-generated method stub
		String message = getHTMLHeader();
		message = message + "<p> Not implemented yet </p>" + getHTMLFooter();
		response.write(message);
	}

	@Override
	public boolean processRequest(DefaultHttpServerConnection serverConnection,
			HttpRequest request, RequestLine requestLine) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String checkAvailability() {
		// TODO Auto-generated method stub
		String message = getHTMLHeader();
		message = message + "<p> " + getMInfo() + "</p>";
		message = message + "<br><br>";
		message = message + "<p> Works! </p>" + getHTMLFooter();
		return message;
	}

	@Override
	public void doGetFile(InputStream f, SroidRequest request,
			SroidResponse response) throws HttpException, IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public HashMap<String, String> getDescriptors() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
