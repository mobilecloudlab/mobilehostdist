//   Copyright 2010 Mobile Cloud Lab, University of Tartu
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package ut.ee.mds;

import java.util.Properties;

import mobile.info.SroidMobInf;
import monitor.movement.Accelerometer;
import network.info.SroidNetwork;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import ut.ee.mds.SroidService;

public class Activator implements BundleActivator {
	private BundleContext m_context = null;
	private ServiceRegistration m_registration = null;

	/**
	 * Implements BundleActivator.start(). Registers an instance of a dictionary
	 * service using the bundle context; attaches properties to the service that
	 * can be queried when performing a service look-up.
	 * 
	 * @param context
	 *            the framework context for the bundle.
	 **/
	public void start(BundleContext context) {
		this.m_context = context;
		Properties props = new Properties();
		props.put("Service", "AndroidGPS");
		m_registration = context.registerService(SroidService.class.getName(),
				new SroidGPS(), props);
		
		props.put("Service", "Notification");
		m_registration = context.registerService(SroidService.class.getName(),
				new Notification(), props);
		
		props.put("Service", "AndroidNetInf");
		m_registration = context.registerService(SroidService.class.getName(),
				new SroidNetwork(), props);
		
		props.put("Service", "AndroidMobInf");
		m_registration = context.registerService(SroidService.class.getName(),
				new SroidMobInf(), props);
		
		props.put("Service", "AndroidAccelerometer");
		m_registration = context.registerService(SroidService.class.getName(),
				new Accelerometer(), props);
	}

	/**
	 * Implements BundleActivator.stop(). Does nothing since the framework will
	 * automatically unregister any registered services.
	 * 
	 * @param context
	 *            the framework context for the bundle.
	 **/
	public void stop(BundleContext context) {
		// NOTE: The service is automatically unregistered.
		// m_registration.unregister();
		m_context = null;
	}

	public BundleContext getContext() {
		return m_context;
	}

	public Bundle[] getBundles() {
		if (m_context != null) {
			return m_context.getBundles();
		}
		return null;
	}

	/**
	 * A private inner class that implements a dictionary service; see
	 * DictionaryService for details of the service.
	 **/
}
