//   Copyright 2010 Mobile Cloud Lab, University of Tartu
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package ut.ee.mds;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.RequestLine;
import org.apache.http.impl.DefaultHttpServerConnection;

import ut.ee.sroid.SroidRequest;
import ut.ee.sroid.SroidResponse;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class Notification implements SroidService {

	private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 1; // in
																		// Meters
	private static final long MINIMUM_TIME_BETWEEN_UPDATES = 1000; // in
																	// Milliseconds

	protected LocationManager locationManager;
	protected Activity activity;
	protected Context context;

	public String getHTMLHeader() {
		return "<html><head><title>Notification</title></head><body>";
	}

	public String getHTMLFooter() {
		return "</body></html>";
	}

	@Override
	public void doCreate(Activity activity, Context context) {
		this.activity = activity;
		this.context = context;
	}
	

	public String LaunchActivity(final String message) {
		try {

			Intent broadcastIntent = new Intent();
			broadcastIntent.setAction("ut.ee.mh.intent.action.SROID_START");
			broadcastIntent.addCategory("ut.ee.async");
			broadcastIntent.putExtra("message", message);
			// activity.sendBroadcast(broadcastIntent);
			context.sendBroadcast(broadcastIntent);
			return "OK " + message;
		} catch (Exception e) {
			Log.v("MobileSroid", e.toString());
		}
		return "Error";
	}

	@Override
	public void doGEt(SroidRequest request, SroidResponse response) {
		// TODO Auto-generated method stub
		String rmsg = request.readParameter("message");
		String message = getHTMLHeader();
		message = message + "<p> " + LaunchActivity(rmsg) + "</p>";
		message = message + "<br><br>";
		message = message + getHTMLFooter();
		response.write(message);
		try {
			response.close();
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			response.close();
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void doPost(SroidRequest request, SroidResponse response)
			throws HttpException, IOException {
		// TODO Auto-generated method stub
		String message = getHTMLHeader();
		message = message + "<p> Not implemented yet </p>" + getHTMLFooter();
		response.write(message);
		response.close();
	}

	@Override
	public void doDelete(SroidRequest request, SroidResponse response) {
		// TODO Auto-generated method stub
		String message = getHTMLHeader();
		message = message + "<p> Not implemented yet </p>" + getHTMLFooter();
		response.write(message);
	}

	@Override
	public void doPut(SroidRequest request, SroidResponse response) {
		// TODO Auto-generated method stub
		String message = getHTMLHeader();
		message = message + "<p> Not implemented yet </p>" + getHTMLFooter();
		response.write(message);

	}

	@Override
	public boolean processRequest(DefaultHttpServerConnection serverConnection,
			HttpRequest request, RequestLine requestLine) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String checkAvailability() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		String message = getHTMLHeader();
		message = message + "<p>  Available </p>";
		message = message + "<br><br>";
		message = message + getHTMLFooter();
		return message;
	}

	@Override
	public void onAccelerationChanged(float x, float y, float z) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onShake(float force) {
		// TODO Auto-generated method stub

	}

	@Override
	public HashMap<String, String> getDescriptors() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		HashMap<String, String> instance = new HashMap<String, String>();
		instance.put(GPS_TAG, "0,0");
		instance.put(ACCELEROMETER_TAG, "0,0,0");
		instance.put(CPU_TAG, "0");
		instance.put(MEMORY_TAG, "64");
		instance.put(SCREEN_TAG, "720,1280");
		instance.put(BANDWITH_TAG, "1024");
		instance.put(CAMERA_TAG, "8");
		instance.put(STORAGE_TAG, "16");
		instance.put(OS_TAG, "android");
		instance.put(OS_VERSION_TAG, "8");
		return instance;
	}

	@Override
	public void doGetFile(InputStream f, SroidRequest request,
			SroidResponse response) throws HttpException, IOException {
		// TODO Auto-generated method stub

	}

}
