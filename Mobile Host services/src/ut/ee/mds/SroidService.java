//   Copyright 2010 Mobile Cloud Lab, University of Tartu
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package ut.ee.mds;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.RequestLine;
import org.apache.http.impl.DefaultHttpServerConnection;

import android.app.Activity;
import android.content.Context;

import ut.ee.sroid.SroidRequest;
import ut.ee.sroid.SroidResponse;

public interface SroidService 
{
    /**
     * Check for the existence of a word.
     * @param word the word to be checked.
     * @return true if the word is in the dictionary,
     *         false otherwise.
    **/
	public void onAccelerationChanged(float x, float y, float z);
	public void onShake(float force);
	public void doCreate(Activity activity, Context context);
	public void doGEt(SroidRequest request, SroidResponse response) throws HttpException, IOException;
	public void doPost(SroidRequest request, SroidResponse response) throws HttpException, IOException;
	public void doDelete(SroidRequest request, SroidResponse response)throws HttpException, IOException;
	public void doPut(SroidRequest request, SroidResponse response) throws HttpException, IOException;
    public boolean processRequest(DefaultHttpServerConnection serverConnection, HttpRequest request,
			RequestLine requestLine);
    public String checkAvailability();
    public void doGetFile(InputStream f,SroidRequest request, SroidResponse response) throws HttpException, IOException;
    public HashMap<String,String> getDescriptors();
    

    static int GPS = 0;
	static int ACCELEROMETER = 1;
	static int CPU = 2;
	static int MEMORY = 3;
	static int SCREEN = 4;
	static int BANDWITH = 5;
	static int CAMERA = 6;
	static int STORAGE = 7;
	static int OS = 8;
	static int OS_VERSION = 9;

	static String GPS_TAG = "gps";
	static String ACCELEROMETER_TAG = "accelerometer";
	static String CPU_TAG = "cpu";
	static String MEMORY_TAG = "memory";
	static String SCREEN_TAG = "screen";
	static String BANDWITH_TAG = "bandwith";
	static String CAMERA_TAG = "camera";
	static String STORAGE_TAG = "storage";
	static String OS_TAG = "os";
	static String OS_VERSION_TAG = "os_version";
	static String TEMPERATURE = "temperature";
}
