//   Copyright 2010 Mobile Cloud Lab, University of Tartu
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package ut.ee.sroid;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.RequestLine;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.DefaultHttpServerConnection;
import org.apache.http.message.BasicHttpResponse;

import android.app.Activity;
import android.content.Context;

import ut.ee.mds.SroidService;

class SroidServiceImplementation implements SroidService {

	public String getHTMLFooter() {
		return "</body></html>";
	}

	public String getHTMLHeader() {
		return "<html><head><title>File Share</title></head><body>";
	}

	private String getLoginForm() {
		return "<form method=\"POST\" action=\"/login\""
				+ " enctype=\"application/x-www-form-urlencoded\""
				+ "/><input type=\"password\" name=\"password\"/>"
				+ "<input type=\"submit\" value=\"Login\"/></form>";
	}

	private void sendLoginForm(DefaultHttpServerConnection serverConnection,
			RequestLine requestLine) throws UnsupportedEncodingException,
			HttpException, IOException {
		HttpResponse response = new BasicHttpResponse(new HttpVersion(1, 1), 200, "OK");
		response.setEntity(new StringEntity(getHTMLHeader()
				+ "<p>Password Required</p>" + getLoginForm()
				+ getHTMLFooter()));
		serverConnection.sendResponseHeader(response);
		serverConnection.sendResponseEntity(response);
	}

	@Override
	public boolean processRequest(
			DefaultHttpServerConnection serverConnection,
			HttpRequest request, RequestLine requestLine) {
		// TODO Auto-generated method stub
		try {
			sendLoginForm(serverConnection, requestLine);
			return true;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public String checkAvailability() {
		// TODO Auto-generated method stub
		return "Everything works as expected!";
	}

	@Override
	public void doGEt(SroidRequest request, SroidResponse response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doPost(SroidRequest request, SroidResponse response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doDelete(SroidRequest request, SroidResponse response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doPut(SroidRequest request, SroidResponse response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doCreate(Activity activity, Context context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAccelerationChanged(float x, float y, float z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onShake(float force) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public HashMap<String, String> getDescriptors() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void doGetFile(InputStream f, SroidRequest request,
			SroidResponse response) throws HttpException, IOException {
		// TODO Auto-generated method stub

	}

}
