//   Copyright 2010 Mobile Cloud Lab, University of Tartu
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package ut.ee.sroid;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.RequestLine;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.DefaultHttpServerConnection;
import org.apache.http.message.BasicHttpResponse;

public class SroidResponse {
	DefaultHttpServerConnection serverConnection;
	HttpRequest request;
	RequestLine requestLine;
	Map<String, String> getParameters = new HashMap<String, String>();
	String message;

	public SroidResponse(DefaultHttpServerConnection serverConnection,
			HttpRequest request, RequestLine requestLine) {
		this.request = request;
		this.serverConnection = serverConnection;
		this.requestLine = requestLine;
		message = "";
	}

	public void write(String text) {
		message = message + text;
	}

	public void close() throws HttpException, IOException {
		HttpResponse response = new BasicHttpResponse(new HttpVersion(1, 1),
				200, "OK");
		response.setEntity(new StringEntity(message));
		response.addHeader("Content-Length",String.valueOf(message.length()));
		serverConnection.sendResponseHeader(response);
		serverConnection.sendResponseEntity(response);
		// serverConnection.flush();
		// serverConnection.close();
	}

}
